import React from "react";
import { projects } from "../constants";
import { arrow } from "../assets/icons";
import { Link } from 'react-router-dom';


const Projects = () => {
    return (
        <section className="max-container">
            My <span className="blue-gradient_text font-semibold drop-shadow">Projects</span>

            <div className="mt-5 flex flex-col gap-3 text-slate-500">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo natus cum pariatur labore sed laudantium, ex odio placeat aut, sapiente obcaecati non officiis saepe. Nisi.</p>
            </div>

            <div className="flex flex-wrap my-20 gap-16">
                {projects.map((project) => (
                    <div className="lg:w-[400px] w-full" key={project.name}>
                        <div className="block-container w-12 h-12">
                            <div className={`btn-back rounded-xl ${project.theme}`}></div>
                            <div className="btn-front flex rounded-xl justify-center items-center">
                                <img src={project.iconUrl} alt="Project icon" className="w-1.2 h-1/2 object-contain" />
                            </div>
                        </div>
                        <div className="mt-5 flex flex-col">
                            <h4 className="text-2xl font-poppins font-semibold">
                                {project.name}
                            </h4>
                            <p className="mt-2 text-slate-500">
                                {project.description}
                            </p>
                            <div className="mt-5 flex items-center gap-2 font-poppins">
                                <Link to={project.link} target="_blank" rel="noopener noreferrer" className='font-semibold text-blue-600'>
                                    live link
                                </Link>
                                <img src={arrow} alt="arrow" className="w-4 h-4 object-contain"/>
                            </div>
                        </div>
                    </div>
                ))}
            </div>

            <hr className="border-slate-200"/>
            
        </section>
    )
}

export default Projects;