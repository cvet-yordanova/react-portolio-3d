import React from 'react';
import { Link } from 'react-router-dom';
import {arrow} from '../assets/icons';

const InfoBox = ({text, link, btnText}) => (
    <div className='info-box'>
        <p className='font-medium sm:text-xl text-center'></p>
        {text}
        <Link to={link} className='neo-brutalism-white neo-btn'>{btnText}
        <img src={arrow} className='w-4 h-4 object-contain'/>
        </Link>
    </div>
)

const renderContent = {
    1: (
        <h1 className="sm:text-xl sm:leading-snug text-center neo-brutalism-blue py-4 px-8 text-white mx-5">
            Hi, I am <span className='font-semibold'>Sandra</span>
         A software engineer from Bulgaria
        </h1>
    ),
    2: (
        <InfoBox text='2 WOrked with many companies and picked many skills along the way' link="/about" btnText="Learn more"/>
    ),
    3: (
        <InfoBox text='3 WOrked with many companies and picked many skills along the way' link="/projects" btnText="Visit my portfolio"/>

    ),
    4: (
        <InfoBox text='4 WOrked with many companies and picked many skills along the way' link="/contact" btnText="Let's talk"/>

    )
}



const HomeInfo = ({currentStage}) => {
    return  renderContent[currentStage] || null;
    
}

export default HomeInfo;