import { meta, shopify, starbucks, tesla } from "../assets/images";
import {
    car,
    contact,
    css,
    estate,
    express,
    git,
    github,
    html,
    javascript,
    linkedin,
    mongodb,
    motion,
    mui,
    nextjs,
    nodejs,
    pricewise,
    react,
    redux,
    sass,
    snapgram,
    summiz,
    tailwindcss,
    threads,
    typescript
} from "../assets/icons";

export const skills = [
    {
        imageUrl: css,
        name: "CSS",
        type: "Frontend",
    },
    {
        imageUrl: express,
        name: "Express",
        type: "Backend",
    },
    {
        imageUrl: git,
        name: "Git",
        type: "Version Control",
    },
    {
        imageUrl: github,
        name: "GitHub",
        type: "Version Control",
    },
    {
        imageUrl: html,
        name: "HTML",
        type: "Frontend",
    },
    {
        imageUrl: javascript,
        name: "JavaScript",
        type: "Frontend",
    },
    {
        imageUrl: motion,
        name: "Motion",
        type: "Animation",
    },
    {
        imageUrl: mui,
        name: "Material-UI",
        type: "Frontend",
    },
    {
        imageUrl: nodejs,
        name: "Node.js",
        type: "Backend",
    },
    {
        imageUrl: react,
        name: "React",
        type: "Frontend",
    },
    {
        imageUrl: redux,
        name: "Redux",
        type: "State Management",
    },
    {
        imageUrl: sass,
        name: "Sass",
        type: "Frontend",
    },
    {
        imageUrl: tailwindcss,
        name: "Tailwind CSS",
        type: "Frontend",
    },
    {
        imageUrl: typescript,
        name: "TypeScript",
        type: "Frontend",
    }
];


export const socialLinks = [
    {
        name: 'Contact',
        iconUrl: contact,
        link: '/contact',
    },
    {
        name: 'GitHub',
        iconUrl: github,
        link: 'https://github.com/YourGitHubUsername',
    },
    {
        name: 'LinkedIn',
        iconUrl: linkedin,
        link: 'https://www.linkedin.com/in/YourLinkedInUsername',
    }
];

export const projects = [
    {
        iconUrl: pricewise,
        theme: 'btn-back-red',
        name: 'Vitae impedit consequatur',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit consequatur, dolor ipsa ex earum labore similique sequi quisquam placeat dolore temporibus minus eos, voluptas cum velit rerum maiores culpa.',
        link: 'https://gitlab.com/cvet-yordanova',
    },
    {
        iconUrl: threads,
        theme: 'btn-back-green',
        name: 'Vitae impedit consequatur',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit consequatur, dolor ipsa ex earum labore similique sequi quisquam placeat dolore temporibus minus eos, voluptas cum velit rerum maiores culpa.',
        link: 'https://gitlab.com/cvet-yordanova',
    },
    {
        iconUrl: car,
        theme: 'btn-back-blue',
        name: 'amet consectetur adipisicing el',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit consequatur, dolor ipsa ex earum labore similique sequi quisquam placeat dolore temporibus minus eos, voluptas cum velit rerum maiores culpa.',
        link: 'https://gitlab.com/cvet-yordanova',
    },
    {
        iconUrl: snapgram,
        theme: 'btn-back-pink',
        name: 'Lorem ipsum dolor sit amet',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit consequatur, dolor ipsa ex earum labore similique sequi quisquam placeat dolore temporibus minus eos, voluptas cum velit rerum maiores culpa.',
        link: 'https://gitlab.com/cvet-yordanova',
    },
    {
        iconUrl: estate,
        theme: 'btn-back-black',
        name: 'Vitae impedit consequatur',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit consequatur, dolor ipsa ex earum labore similique sequi quisquam placeat dolore temporibus minus eos, voluptas cum velit rerum maiores culpa.',
        link: 'https://gitlab.com/cvet-yordanova',
    },
    {
        iconUrl: summiz,
        theme: 'btn-back-yellow',
        name: 'amet consectetur adipisicing el',
        description: 'lorem30',
        link: 'https://gitlab.com/cvet-yordanova',
    }
];